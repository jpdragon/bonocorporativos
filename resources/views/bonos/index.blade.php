@extends('app')

@section('header')
    Bonos
@endsection

@section('index')
    <li class="active">
        <i class="fa fa-money"></i> Bonos
    </li>
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if($tipoUsuario == 'Emisor')
                            <a href="{{ action('BonosController@create') }}" class="col-md4 btn btn-link">Crear
                                Bono</a>
                        @endif
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <div style="overflow: auto;">
                            <table style="text-align: center;" class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Editar</th>
                                    <th>Valor Nominal</th>
                                    <th>Valor Comercial</th>
                                    <th>Numero de años</th>
                                    <th>Fecha Inicio</th>
                                    <th>Dias por periodo</th>
                                    <th>Dias por año</th>
                                    <th>Periodos por año</th>
                                    <th>Total de Periodos</th>
                                    <th>Impuesto Renta %</th>
                                    <th>% Prima</th>
                                    <th>% Estructuracion</th>
                                    <th>% Colocación</th>
                                    <th>% Flotación</th>
                                    <th>% CAVALI</th>
                                    <th>TEA</th>
                                    <th>TEP</th>
                                    <th>Tasa Descuento</th>
                                    <th>Tasa Periodo</th>
                                    <th>TIR Emisor</th>
                                    <th>TCEA Emisor</th>
                                    <th>TIR Emisor Escudo</th>
                                    <th>TCEA Emisor Escudo</th>
                                    <th>TIR Bonista</th>
                                    <th>TREA Bonista</th>
                                    <th>Precio Actual</th>
                                    <th>VNA</th>
                                    <th>Duracion</th>
                                    <th>Convexidad</th>
                                    <th>Total (Duración + Convexidad)</th>
                                    <th>Duración modificada</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($Bonos as $bono)
                                    <tr>
                                        <td><a href="{{ action('BonosController@show',[$bono->id])}}">Detalle</a></td>
                                        <td>{{ round($bono->valor_nominal,2) }}</td>
                                        <td>{{ round($bono->valor_comercial,2) }}</td>
                                        <td>{{ $bono->num_año }}</td>
                                        <td>{{ $bono->fecha_inicio }}</td>
                                        <td>{{ $bono->dias_periodo }}</td>
                                        <td>{{ $bono->dias_año }}</td>
                                        <td>{{ $bono->periodos_año }}</td>
                                        <td>{{ $bono->total_periodos }}</td>
                                        <td>{{ round($bono->imp_renta*100,2) }} %</td>
                                        <td>{{ round($bono->prima*100,3) }} %</td>
                                        <td>{{ round($bono->estructuracion*100,3) }} %</td>
                                        <td>{{ round($bono->colocacion *100,3)}} %</td>
                                        <td>{{ round($bono->flotacion*100,3) }} %</td>
                                        <td>{{ round($bono->cavali*100,3) }} %</td>
                                        <td>{{ round($bono->tea*100,3) }} %</td>
                                        <td>{{ round($bono->tep*100,3) }} %</td>
                                        <td>{{ round($bono->tasa_descuento*100,3) }} %</td>
                                        <td>{{ round($bono->tasa_periodo*100,3) }} %</td>
                                        <td>{{ round($bono->tir_emisor*100,5) }} %</td>
                                        <td>{{ round($bono->tcea_emisor*100,5) }} %</td>
                                        <td>{{ round($bono->tir_emisor_cEscudo*100,5) }} %</td>
                                        <td>{{ round($bono->tcea_emisor_cEscudo*100,5) }} %</td>
                                        <td>{{ round($bono->tir_bonista*100,5) }} %</td>
                                        <td>{{ round($bono->trea_bonista*100,5) }} %</td>
                                        <td>{{ round($bono->precio_actual,2) }}</td>
                                        <td>{{ round($bono->vna,2) }}</td>
                                        <td>{{ round($bono->duracion,2) }}</td>
                                        <td>{{ round($bono->convexidad,2) }}</td>
                                        <td>{{ round($bono->duracion_convexidad,2) }}</td>
                                        <td>{{ round($bono->duracion_modificada,2) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
