@extends('app')

@section('header')
    Bono
    <small>Usuarios - Bono</small>
@endsection

@section('index')
    <li>
        <i class="fa fa-money"></i> <a href="{{ action('BonosController@index') }}">Bonos</a>
    </li>
    <li>
        <i class="fa fa-table"></i> <a href="{{ action('BonosController@show',['id'=>$id]) }}">Bono</a>
    </li>
    <li class="active"><i class="fa fa-users"></i> Usuarios Bono</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="h2">Bonos</h2>
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <div style="overflow: auto; height: 100%;">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <td>Correo</td>
                                    <td>Eliminar</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($userBono as $ub)
                                    <tr>
                                        <td>{{ $ub->User->email }}</td>
                                        <td>
                                            <a href="{{ action('UsuarioBonoController@destroy',[$ub->id_bono, $ub->id_user]) }}"><i
                                                        class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-offset-2 col-md-8 panel-default">
                            <form role="form" method="POST"
                                  action="{{ action('UsuarioBonoController@store',['id' => $id]) }}">
                                <fieldset>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group col-md-8">
                                        <input type="text" class="form-control" name="email" placeholder="Email"
                                               value="{{ old('email') }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary">
                                                Agregar Usuario
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
