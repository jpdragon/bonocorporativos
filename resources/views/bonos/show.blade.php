@extends('app')

@section('header')
    Bono
    <small>Detallado</small>
@endsection

@section('index')
    <li>
        <i class="fa fa-money"></i> <a href="{{ action('BonosController@index') }}">Bonos</a>
    </li>
    <li class="active"><i class="fa fa-table"></i> Detalle de Bono</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        @if($tipoUsuario == 'Emisor')
                            <a href="{{ action('UsuarioBonoController@index',[$bono->id]) }}"
                               class="col-md-4 btn btn-link">Usuarios Asiociados</a>
                            <div class="clearfix"></div>
                            <hr>
                        @endif
                        <h3 class="h3" style="text-align: center;">Bono</h3>

                        <div style="overflow: auto" class="col-md-11">
                            <table class="bono-table ">
                                <tr>
                                    <th class="col-md-3">Valor Nominal</th>
                                    <td class="col-md-1">{{ round($bono->valor_nominal,2) }}</td>
                                    <th class="col-md-3">Valor Comercial</th>
                                    <td class="col-md-1">{{ round($bono->valor_comercial,2) }}</td>
                                    <th class="col-md-3">Numero de años</th>
                                    <td class="col-md-1">{{ $bono->num_año }}</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">Fecha Inicio</th>
                                    <td class="col-md-1">{{ $bono->fecha_inicio }}</td>
                                    <th class="col-md-3">Dias por periodo</th>
                                    <td class="col-md-1">{{ $bono->dias_periodo }}</td>
                                    <th class="col-md-3">Dias por años</th>
                                    <td class="col-md-1">{{ $bono->dias_año }}</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">Periodos por año</th>
                                    <td class="col-md-1">{{ $bono->periodos_año }}</td>
                                    <th class="col-md-3">Total de Periodos</th>
                                    <td class="col-md-1">{{ $bono->total_periodos }}</td>
                                    <th class="col-md-3">Impuesto Renta</th>
                                    <td class="col-md-1">{{ round($bono->imp_renta*100,3) }}%</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">% Prima</th>
                                    <td class="col-md-1">{{ round($bono->prima*100,3) }}%</td>
                                    <th class="col-md-3">% Estructuracion</th>
                                    <td class="col-md-1">{{ round($bono->estructuracion*100,3) }}%</td>
                                    <th class="col-md-3">% Colocación</th>
                                    <td class="col-md-1">{{ round($bono->colocacion*100,3) }}%</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">% Flotación</th>
                                    <td class="col-md-1">{{ round($bono->flotacion*100,3) }}%</td>
                                    <th class="col-md-3">% CAVALI</th>
                                    <td class="col-md-1">{{ round($bono->cavali*100,3) }}%</td>
                                    <th class="col-md-3">TEA</th>
                                    <td class="col-md-1">{{ round($bono->tea*100,3) }}%</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">TEP</th>
                                    <td class="col-md-1">{{ round($bono->tep*100,3) }}</td>
                                    <th class="col-md-3">Tasa Descuento</th>
                                    <td class="col-md-1">{{ round($bono->tasa_descuento*100,3) }}%</td>
                                    <th class="col-md-3">Tasa Periodo</th>
                                    <td class="col-md-1">{{ round($bono->tasa_periodo*100,3) }}%</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">TIR Emisor</th>
                                    <td class="col-md-1">{{ round($bono->tir_emisor*100,5) }}%</td>
                                    <th class="col-md-3">TCEA Emisor</th>
                                    <td class="col-md-1">{{ round($bono->tcea_emisor*100,5) }}%</td>
                                    <th class="col-md-3">TIR Emisor Escudo</th>
                                    <td class="col-md-1">{{ round($bono->tir_emisor_cEscudo*100,5) }}%</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">TCEA Emisor Escudo</th>
                                    <td class="col-md-1">{{ round($bono->tcea_emisor_cEscudo*100,5) }}%</td>
                                    <th class="col-md-3">TIR Bonista</th>
                                    <td class="col-md-1">{{ round($bono->tir_bonista*100,5) }}%</td>
                                    <th class="col-md-3">TREA Bonista</th>
                                    <td class="col-md-1">{{ round($bono->trea_bonista*100,5) }}%</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">Precio Actual</th>
                                    <td class="col-md-1">{{ round($bono->precio_actual,2) }}</td>
                                    <th class="col-md-3">VNA</th>
                                    <td class="col-md-1">{{ round($bono->vna,2) }}</td>
                                    <th class="col-md-3">Duracion</th>
                                    <td class="col-md-1">{{ round($bono->duracion,2) }}</td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">Convexidad</th>
                                    <td class="col-md-1">{{ round($bono->convexidad,2) }}</td>
                                    <th class="col-md-3">Total</th>
                                    <td class="col-md-1">{{ round($bono->duracion_convexidad,2) }}</td>
                                    <th class="col-md-3">Duración modificada</th>
                                    <td class="col-md-1">{{ round($bono->duracion_modificada,2) }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <hr>
                        <br>

                        <h3 class="h3" style="text-align: center;">Cuotas</h3>

                        <div style="overflow: auto;
                       height: 100%;" class="col-md-12">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Inflación Proyectada</th>
                                    <th>IEP</th>
                                    <th>Bono</th>
                                    <th>Bono Indexado</th>
                                    <th>Plazo de Gracia</th>
                                    <th>Cupon (interes)</th>
                                    <th>Cuota</th>
                                    <th>Amortización</th>
                                    <th>Prima</th>
                                    <th>Escudo</th>
                                    <th>Flujo Emisor</th>
                                    <th>Flujo Emisor Escudo</th>
                                    <th>Flujo Bonista</th>
                                    <th>Flujo Act.</th>
                                    <th>FA x Plazo</th>
                                    <th>Factor p/Convexidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($detalles as $detalle)
                                    <tr>
                                        <td>{{ $detalle->num_periodo }}</td>
                                        <td>{{ round($detalle->inflacion_proyectada*100,2) }}%</td>
                                        <td>{{ round($detalle->IEP*100,2) }}%</td>
                                        <td>{{ round($detalle->bono,2) }}</td>
                                        <td>{{ round($detalle->bono_indexado,2) }}</td>
                                        <td>{{ $detalle->plazo_gracia }}</td>
                                        <td>{{ round($detalle->cupon_interes,2) }}</td>
                                        <td>{{ round($detalle->cuota,2) }}</td>
                                        <td>{{ round($detalle->amortizacion,2) }}</td>
                                        <td>{{ round($detalle->prima,2) }}</td>
                                        <td>{{ round($detalle->escudo,2) }}</td>
                                        <td>{{ round($detalle->flujo_emisor,2) }}</td>
                                        <td>{{ round($detalle->flujo_emisor_cEscudo,2) }}</td>
                                        <td>{{ round($detalle->flujo_bonista,2) }}</td>
                                        <td>{{ round($detalle->flujo_activo,2) }}</td>
                                        <td>{{ round($detalle->fa_plazo,2) }}</td>
                                        <td>{{ round($detalle->factor_convexidad,2) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Error: </strong>Se econtraron algunos problemas en los datos.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if($tipoUsuario == 'Emisor' && $bono->total_periodos > $bono->periodos_ingresados)
                            <div class="clearfix"></div>
                            <hr><br><br>
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ action('DetalleBonoController@store',[$bono->id]) }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Inflación proyectada</label>

                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <input type="number" step="any" class="form-control"
                                                   name="inflacion_proyectada"
                                                   value="{{ old('inflacion_proyectada') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Plazo de Gracia</label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="plazo_gracia" size="1" required>
                                            <option value="T">T</option>
                                            <option value="S">S</option>
                                            <option value="P">P</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Crear Nuevo Detalle
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
