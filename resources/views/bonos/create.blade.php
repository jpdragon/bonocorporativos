@extends('app')


@section('header')
    Bono <small>Crear Bono</small>
@endsection

@section('index')
    <li>
        <i class="fa fa-money"></i> <a href="{{ action('BonosController@index') }}">Bonos</a>
    </li>
    <li class="active"><i class="fa fa-table"></i> Crear Bono</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Error: </strong>Se econtraron algunos problemas en los datos.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ action('BonosController@store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Valor Nominal</label>
                                <div class="col-md-6">
                                    <input type="number" step="any" class="form-control" name="valor_nominal" value="{{ old('valor_nominal') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Valor Comercial</label>
                                <div class="col-md-6">
                                    <input type="number" step="any" class="form-control" name="valor_comercial" value="{{ old('valor_comercial') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Fecha de Inicio</label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="fecha_inicio" value="{{ old('fecha_inicio') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Cantidad de Años</label>
                                <div class="col-md-6">
                                    <input type="number" step="any" class="form-control" name="num_año" value="{{ old('num_año') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Días por Año</label>
                                <div class="col-md-6">
                                    <input type="number" step="any" class="form-control" name="dias_año" value="{{ old('dias_año') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Días por Periodo</label>
                                <div class="col-md-6">
                                    <input type="number" step="any" class="form-control" name="dias_periodo" value="{{ old('dias_periodo') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Total de Periodos</label>
                                <div class="col-md-6">
                                    <input type="number" step="any" class="form-control" name="total_periodos" value="{{ old('total_periodos') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Impuesto a la Renta</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="imp_renta" value="{{ old('imp_renta') }}">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Prima</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="prima" value="{{ old('prima') }}">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Estructuración</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="estructuracion" value="{{ old('estructuracion') }}"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Colocación</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="colocacion" value="{{ old('colocacion') }}"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Flotación</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="flotacion" value="{{ old('flotacion') }}">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Cavalí</label>
                                <div class="col-md-6">

                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="cavali" value="{{ old('cavali') }}">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">TEA</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="tea" value="{{ old('tea') }}">
                                        </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Tasa de Descuento</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                    <input type="number" step="any" class="form-control" name="tasa_descuento" value="{{ old('tasa_descuento') }}">
                                        </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary col-md-6 col-md-offset-6">
                                        Crear Bono
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
