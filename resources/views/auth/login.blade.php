@extends('login_register')

@section('content')

    <div class="row">

        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Autenticar</h3>
                </div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <!--<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                                <strong>Error: </strong><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                    <form role="form" method="POST" action="{{ url('/auth/login') }}">
                        <fieldset>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <input type="email" placeholder="E-mail" class="form-control" name="email"
                                       value="{{ old('email') }}">
                            </div>

                            <div class="form-group">
                                <input type="password" placeholder="Contraseña" class="form-control" name="password">
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Recordarme
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-success btn-block">Autenticar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <a style="float: right; position: relative; top: -15px;" href="{{ url('/auth/register') }}"><button type="button" class="btn btn-link">Resgistrarse</button></a>
        </div>
    </div>
@endsection