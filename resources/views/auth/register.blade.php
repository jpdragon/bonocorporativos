@extends('login_register')



@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="register-panel panel panel-default">
                    <div class="panel-heading">Registrarse</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Error: </strong><br/>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form role="form" method="POST" action="{{ url('/auth/register') }}">
                            <fieldset>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nombre</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nombre"
                                               value="{{ old('nombre') }}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Apellidos</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="apellidos"
                                               value="{{ old('apellidos') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Direccion</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="direccion"
                                               value="{{ old('direccion') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Ciudad</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="ciudad"
                                               value="{{ old('ciudad') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Pais</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="pais" value="{{ old('pais') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Fecha de Nacimiento</label>

                                    <div class="col-md-8">
                                        <input type="date" class="form-control" name="fecha_nacimiento"
                                               value="{{ old('fecha_nacimiento') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Sexo</label>

                                    <div class="col-md-8">
                                        <label class="col-md-6 radio-btn" ><input class="col-md-2" type="radio" name="sexo"
                                               value="masculino" @if(old('sexo') == 'masculino' || old('sexo') == '')
                                               checked @endif>Masculino</label>
                                        <label class="col-md-4 radio-btn" ><input class="col-md-2" type="radio" name="sexo"
                                               value="femenino" @if(old('sexo') == 'femenino') checked @endif>Femenino</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Contraseña</label>

                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Confirmar Contraseña</label>

                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="password_confirmation">
                                    </div>
                                </div>
<br>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary col-md-offset-1 col-md-10">Registrarse</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <a style="float: right; position: relative; top: -15px;" href="{{ url('/auth/login') }}"><button type="button" class="btn btn-link">Ya eres usuario?</button></a>
            </div>
        </div>
    </div>
@endsection
