<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe ser de al menos 6 caracteres y ser igual a la confimarción.',
    'user' => "No se encontró un usuario con el email ingresado.",
    'token' => 'Este "token" para la contraseña no es valido. ',
    'sent' => 'Se ha enviado el email de confimarción! ',
    'reset' => 'Tu contraseña ha sido reiniciada!',

];
