<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBono extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bono', function (Blueprint $table) {

            $table->increments('id')->length(10)->unsigned();
            $table->integer('id_user')->length(10)->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->integer('id_bono')->length(10)->unsigned();
            $table->foreign('id_bono')->references('id')->on('bono');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bono');
    }
}
