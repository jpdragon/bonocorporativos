<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBono extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bono', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();

            $table->decimal('valor_nominal', 10, 5)->nullable();
            $table->decimal('valor_comercial', 10, 5)->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->integer('num_año')->nullable();
            $table->integer('dias_periodo')->nullable();
            $table->integer('dias_año')->nullable();
            $table->integer('periodos_año')->nullable();//
            $table->integer('total_periodos')->nullable();//
            $table->integer('periodos_ingresados')->nullable();
            $table->decimal('imp_renta', 10, 5)->nullable();
            $table->decimal('prima', 10, 5)->nullable();
            $table->decimal('estructuracion', 10, 5)->nullable();
            $table->decimal('colocacion', 10, 5)->nullable();
            $table->decimal('flotacion', 10, 5)->nullable();
            $table->decimal('cavali', 10, 5)->nullable();
            $table->decimal('tea', 10, 5)->nullable();
            $table->decimal('tep', 10, 5)->nullable();//
            $table->decimal('tasa_descuento', 10, 5)->nullable();
            $table->decimal('tasa_periodo', 10, 5)->nullable();//

            $table->decimal('tir_emisor', 10, 5)->nullable();
            $table->decimal('tcea_emisor', 10, 5)->nullable();
            $table->decimal('tir_emisor_cEscudo', 10, 5)->nullable();
            $table->decimal('tcea_emisor_cEscudo', 10, 5)->nullable();

            $table->decimal('trea_bonista', 10, 5)->nullable();
            $table->decimal('tir_bonista', 10, 5)->nullable();

            $table->decimal('precio_actual', 10, 5)->nullable();//
            $table->decimal('vna', 10, 5)->nullable();//
            $table->decimal('duracion', 10, 5)->nullable();//
            $table->decimal('convexidad', 10, 5)->nullable();//
            $table->decimal('duracion_convexidad', 10, 5)->nullable();//
            $table->decimal('duracion_modificada', 10, 5)->nullable();//




            //$table->integer('id_usuario')->unsigned();
            //$table->foreign('id_usuario')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bono');
    }
}
