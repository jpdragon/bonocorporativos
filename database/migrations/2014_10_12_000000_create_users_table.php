<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Email y password son propios de autenticacion - No cambiar
         */
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id')->length(10)->unsigned();

            $table->string('nombre', 45);
            $table->string('apellidos',45);
            $table->string('direccion', 60);
            $table->string('ciudad', 45);
            $table->string('pais', 45);
            $table->date('fecha_nacimiento');
            $table->binary('sexo');

            /*
            $table->integer('id_tipo');
            $table->foreign('id_tipo')->references('id')->on('tipo_usuario')->onDelete('cascade');
            */
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
