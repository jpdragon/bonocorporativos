<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_usuario', function (Blueprint $table) {

            $table->increments('id')->length(10)->unsigned();

            $table->string('tipo_usuario', 45);

            $table->timestamps();
        });
        Schema::table('users', function(Blueprint $table)
        {
            $table->integer('id_tipo')->length(10)->unsigned();
            $table->foreign('id_tipo')->references('id')->on('tipo_usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_usuario');
    }
}
