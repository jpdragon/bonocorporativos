<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleBono extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_bono', function (Blueprint $table) {
            $table->increments('id')->length(10)->unsigned();

            $table->integer('num_periodo')->nullable();
            $table->decimal('inflacion_proyectada', 10, 5)->nullable();
            $table->char('plazo_gracia',1)->nullable();
            $table->decimal('IEP', 10, 5)->nullable();
            $table->decimal('bono', 10, 5)->nullable();
            $table->decimal('bono_indexado', 10, 5)->nullable();
            $table->decimal('amortizacion', 10, 5)->nullable();
            $table->decimal('prima', 10, 5)->nullable();
            $table->decimal('escudo', 10, 5)->nullable();
            $table->decimal('flujo_activo', 10, 5)->nullable();
            $table->decimal('fa_plazo', 10, 5)->nullable();
            $table->decimal('factor_convexidad', 10, 5)->nullable();
            $table->decimal('cupon_interes', 10, 5)->nullable();
            $table->decimal('cuota', 10, 5)->nullable();

            $table->decimal('flujo_emisor', 10, 5)->nullable();
            $table->decimal('flujo_emisor_cEscudo', 10, 5)->nullable();

            $table->decimal('flujo_bonista', 10, 5)->nullable();

            //$table->decimal('total_prima', 10, 2)->nullable();

            //FALTAN FK


            $table->integer('id_bono')->length(10)->unsigned();
            $table->foreign('id_bono')->references('id')->on('bono');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_bono');
    }
}
