<?php

use Illuminate\Database\Seeder;

class TipoUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('tipo_usuario')->insert([
            'tipo_usuario' => 'Bonista'
            //'created_at' => this.time(),/
        ]);
        DB::table('tipo_usuario')->insert([
            'tipo_usuario' => 'Emisor'
        ]);
    }
}
