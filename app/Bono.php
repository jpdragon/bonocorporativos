<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bono extends Model
{
    //
    protected $table = 'bono';

    protected $fillable =[
        'valor_nominal',
        'valor_comercial',
        'fecha_incio',
        'num_año',
        'dias_periodo',
        'dias_año',
        'periodos_año',
        'total_periodos',
        'imp_renta',
        'prima',
        'estructuracion',
        'colocacion',
        'flotacion',
        'cavali',
        'tea',
        'tep',
        'tasa_descuento',
        'tasa_periodo',
        'precio_actual',
        'vna',
        'duracion',
        'convexividad',
        'duracion_convexividad',
        'duracion_modificada',
        ];

    public function Users()
    {
        return $this->belongsToMany('App\User', 'user_bono', 'id_bono', 'id_user');
    }

    public function DetalleBono()
    {
        return $this->hasMany('App\DetalleBono','id_bono')->orderBy('num_periodo','asc');
    }
}
