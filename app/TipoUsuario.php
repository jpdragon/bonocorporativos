<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    //
    protected $table = 'tipo_usuario';
    protected $fillable = ['tipo_usuario'];

    public function users()
    {
        return $this->hasMany('App\User','id_tipo','id');
    }

}
