<?php
/**
 * Created by PhpStorm.
 * User: Jean Pierre
 * Date: 17/06/2015
 * Time: 08:22 AM
 */

namespace app\Services;


use App\Bono;
use App\DetalleBono;
use App\UserBono;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Request;

class BonoService
{

    /**
     * @param array $data
     */
    public function RegisterBono(array $data)
    {
        $bono = new Bono;
        $bono->valor_nominal = $data['valor_nominal'];
        $bono->valor_comercial = $data['valor_comercial'];
        $bono->fecha_inicio = $data['fecha_inicio'];
        $bono->num_año = $data['num_año'];
        $bono->dias_año = $data['dias_año'];
        $bono->dias_periodo = $data['dias_periodo'];
        $bono->total_periodos = $data['total_periodos'];
        $bono->imp_renta = $data['imp_renta']/100;
        $bono->prima = $data['prima']/100;
        $bono->estructuracion = $data['estructuracion']/100;
        $bono->colocacion = $data['colocacion']/100;
        $bono->flotacion = $data['flotacion']/100;
        $bono->cavali = $data['cavali']/100;
        $bono->tasa_descuento = $data['tasa_descuento']/100;
        $bono->tea = $data['tea']/100;

        self::CalculateBonoFirstTime($bono);

        $bono->save();
        $user_bono = new UserBono;
        $user_bono->id_bono = $bono->id;
        $user_bono->id_user = Auth::user()->id;
        $user_bono->save();
    }

    public function RegisterDetalleBono(array $data, Bono $bono)
    {
        $detalleBono = new DetalleBono;
        $detalleBono->inflacion_proyectada = $data['inflacion_proyectada']/100;
        $detalleBono->plazo_gracia = $data['plazo_gracia'];
        $detalleBono->num_periodo = $bono->periodos_ingresados + 1;
        $detalleBono->id_bono = $bono->id;
        self::CalculateDetalleBono($bono, $detalleBono);
        $bono->periodos_ingresados = $bono->periodos_ingresados + 1;
        $detalleBono->save();

        if ($detalleBono->num_periodo == $bono->total_periodos)
            self::RecalculateBono($bono);
        //$detalleBono
        $bono->save();
    }

    protected function CalculateBonoFirstTime(Bono $bono)
    {
        $bono->periodos_año = $bono->dias_año / $bono->dias_periodo;
        $bono->periodos_ingresados = 0;
        $bono->tep = pow(1 + $bono->tea, $bono->dias_periodo / $bono->dias_año) - 1;
        $bono->tasa_periodo = pow(1 + $bono->tasa_descuento, $bono->dias_periodo / $bono->dias_año) - 1;
        $bono->precio_actual = 0;
        $bono->save();
        $detalleBono = self::CalculateFirstDetalleBono($bono);
        $bono->vna = $detalleBono->flujo_bonista;
    }

    /**
     * @param Bono $bono
     */
    protected function RecalculateBono(Bono $bono)
    {
        $primer_detalle = $bono->DetalleBono->first();
        $detalles = $bono->DetalleBono()->whereNotIn('num_periodo', [0])->get();
        Log::info($detalles);
        //Log::info($primer_detalle);
        //$detalles = DB::table('detalle_bono')->where('votes', '=', 100)->get();
        $bono->tir_emisor = self::TIR($primer_detalle->flujo_emisor, $detalles, 'flujo_emisor');
        $bono->tcea_emisor = pow(1 + $bono->tir_emisor, $bono->periodos_año) - 1;
        $bono->tir_emisor_cEscudo = self::TIR($primer_detalle->flujo_emisor_cEscudo, $detalles, 'flujo_emisor_cEscudo');
        $bono->tcea_emisor_cEscudo = pow(1 + $bono->tir_emisor_cEscudo, $bono->periodos_año) - 1;
        $bono->tir_bonista = self::TIR($primer_detalle->flujo_bonista, $detalles, 'flujo_bonista');
        $bono->trea_bonista = pow(1 + $bono->tir_bonista, $bono->periodos_año) - 1;
        $bono->precio_actual = self::VAN($bono->tasa_periodo, $detalles, 'flujo_bonista');
        $bono->vna = $primer_detalle->flujo_bonista + $bono->precio_actual;
        $bono->duracion = self::Duracion($detalles);
        $bono->convexidad = self::Convexidad($bono, $detalles);
        $bono->duracion_convexidad = $bono->duracion + $bono->convexidad;
        $bono->duracion_modificada = $bono->duracion / (1 + $bono->tasa_periodo);

    }

    protected function CalculateFirstDetalleBono(Bono $bono)
    {
        $detalleBono = new DetalleBono;
        $detalleBono->id_bono = $bono->id;
        $detalleBono->num_periodo = 0;
        $detalleBono->flujo_emisor = $bono->valor_comercial - ($bono->valor_comercial * ($bono->colocacion + $bono->estructuracion + $bono->flotacion + $bono->cavali));
        $detalleBono->flujo_emisor_cEscudo = $detalleBono->flujo_emisor;
        $detalleBono->flujo_bonista = -$bono->valor_comercial - ($bono->valor_comercial * ($bono->flotacion + $bono->cavali));


        $detalleBono->save();
        return $detalleBono;
    }

    /**
     * @param Bono $bono
     * @param DetalleBono $detalleBono
     */
    protected function CalculateDetalleBono(Bono $bono, DetalleBono $detalleBono)
    {
        $detalleBonoAnterior = $bono->DetalleBono->where('num_periodo', $bono->periodos_ingresados)->first();

        $detalleBono->IEP = pow(1 + $detalleBono->inflacion_proyectada, $bono->dias_periodo / $bono->dias_año) - 1;
        //BONO
        if ($detalleBono->num_periodo == 1) {
            $detalleBono->bono = $bono->valor_nominal;
        } else {
            if ($detalleBonoAnterior->plazo_gracia == 'T') {
                $detalleBono->bono = $detalleBonoAnterior->bono_indexado - $detalleBonoAnterior->cupon_interes;
            } else {
                $detalleBono->bono = $detalleBonoAnterior->bono_indexado + $detalleBonoAnterior->amortizacion;
            }
        }
        //BONO INDEXADO
        $detalleBono->bono_indexado = $detalleBono->bono * (1 + $detalleBono->IEP);
        //CUPON
        $detalleBono->cupon_interes = -1 * $detalleBono->bono_indexado * $bono->tep;

        if ($detalleBono->plazo_gracia == 'S') {
            $detalleBono->amortizacion = (-1 * $detalleBono->bono_indexado) / ($bono->total_periodos - $detalleBono->num_periodo + 1);
            $detalleBono->cuota = $detalleBono->cupon_interes + $detalleBono->amortizacion;
        } else if ($detalleBono->plazo_gracia == 'P') {
            $detalleBono->cuota = $detalleBono->cupon_interes;
        } else {
            $detalleBono->cuota = 0;
        }

        //ESCUDO
        $detalleBono->escudo = -1 * $detalleBono->cupon_interes * $bono->imp_renta;

        //Flujo Emisor
        if ($detalleBono->num_periodo == $bono->total_periodos) {
            $detalleBono->prima = -1 * $detalleBono->bono_indexado * $bono->prima;
            $detalleBono->flujo_emisor = $detalleBono->cuota + ($detalleBono->prima);
        } else {
            $detalleBono->flujo_emisor = $detalleBono->cuota;
        }

        //FLUJO EMISOR CON ESCUDO
        $detalleBono->flujo_emisor_cEscudo = $detalleBono->flujo_emisor + $detalleBono->escudo;

        //FLUJO BONISTA;
        $detalleBono->flujo_bonista = -1 * $detalleBono->flujo_emisor;

        //FLUJO ACTIVO
        $detalleBono->flujo_activo = ($detalleBono->flujo_bonista) / (pow($bono->tasa_periodo + 1, $detalleBono->num_periodo));

        //FA X PLAZO
        $detalleBono->fa_plazo = $detalleBono->flujo_activo * $detalleBono->num_periodo * ($bono->dias_periodo / $bono->dias_año);
        //FACTOR P CONVEXIDAD
        $detalleBono->factor_convexidad = $detalleBono->flujo_activo * $detalleBono->num_periodo * (1 + $detalleBono->num_periodo);

    }

    /** Calcula el VAN
     * @param $tasa_aplicable
     * @param array $flujo
     * @return int
     */
    protected function VAN($tasa_aplicable, $flujo, $campo)
    {
        $suma = 0.00;
        foreach ($flujo as $dato) {
            if ($campo == 'flujo_emisor')
                $suma = $suma + ($dato->flujo_emisor / (pow(1 + $tasa_aplicable, $dato->num_periodo)));
            else if ($campo == 'flujo_bonista')
                $suma = $suma + ($dato->flujo_bonista / (pow(1 + $tasa_aplicable, $dato->num_periodo)));
            else
                $suma = $suma + ($dato->flujo_emisor_cEscudo / (pow(1 + $tasa_aplicable, $dato->num_periodo)));
        }
        return $suma;
    }

    /** Calcula el TIR
     * @param $monto_inicial
     * @param array $flujo
     * @return float
     */
    protected function TIR($monto_inicial, $flujo, $campo)
    {
        $maximo = 2.0;
        $minimo = -1.0;
        $tir = 0.0;
        $van_flujo = 0.0;
        $inicio = time();
        do {
            if(time() - $inicio > 8)
            {
                $tir = null;
                break;
            }
            $tir = ($maximo + $minimo) / 2;
            $van_flujo = self::VAN($tir, $flujo, $campo);
            //Log::info($van_flujo);
            //Log::info($monto_inicial);

            if (abs($van_flujo) < abs($monto_inicial)) {
                $maximo = $tir;
            } else {
                $minimo = $tir;
            }
            $van_flujo = abs($van_flujo + $monto_inicial);
            //Log::info($van_flujo);

        } while ($van_flujo > 0.000001);

        return $tir;
    }

    protected function Duracion($flujo)
    {
        $suma_faplazo = 0.00;
        $suma_fact = 0.00;
        foreach ($flujo as $dato) {
            $suma_fact = $suma_fact + $dato->flujo_activo;
            $suma_faplazo = $suma_faplazo + $dato->fa_plazo;
        }
        return ($suma_fact != 0) ? $suma_faplazo / $suma_fact : 0;
    }

    protected function Convexidad(Bono $bono, $flujo)
    {
        $suma_factorPConvexidad = 0.00;
        $suma_fact = 0.00;
        foreach ($flujo as $dato) {
            $suma_fact = $suma_fact + $dato->flujo_activo;
            $suma_factorPConvexidad = $suma_factorPConvexidad + $dato->factor_convexidad;
        }

        return ($suma_fact != 0 && $bono->tasa_periodo != 0) ? $suma_factorPConvexidad / (pow(1 + $bono->tasa_periodo, 2) * $suma_fact * pow($bono->periodos_año, 2)) : 0;
    }

}