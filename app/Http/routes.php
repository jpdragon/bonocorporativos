<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'BonosController@index');

Route::get('home', 'BonosController@index');

//Route::resource('bonos','BonoController');
//Route::get('bonos', 'BonoController@index');

Route::get('bonos', 'BonosController@index');

Route::group(['middleware' => 'emisor'], function () {

    Route::get('bonos/create', 'BonosController@create');
    Route::post('bonos/store', 'BonosController@store');

    Route::post('bonos/{id}/store', 'DetalleBonoController@store');

    Route::get('bonos/{id_bono}/usuarios', 'UsuarioBonoController@index');
    Route::post('bonos/{id_bono}/usuarios/store', 'UsuarioBonoController@store');
    Route::get('bonos/{id_bono}/usuarios/destroy/{id_user}', 'UsuarioBonoController@destroy');
    //ver los usuarios asociados
    //añadir un usuario a un bono asociado
    //destruit un asociacion usuario-bono
});


Route::get('bonos/{id}', 'BonosController@show');
/*
    GET 	    /photo	                    index       photo.index
    GET	        /photo/create	            create      photo.create
    POST	    /photo	                    store       photo.store
    GET	        /photo/{photo}      	    show        photo.show
    GET	        /photo/{photo}/edit	        edit        photo.edit
    PUT         /PATCH	/photo/{photo}	    update      photo.update
    DELETE	    /photo/{photo}	            destroy     photo.destroy
 */


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');