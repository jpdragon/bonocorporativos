<?php

namespace App\Http\Middleware;

use App\User;
use App\TipoUsuario;
use Closure;
use Illuminate\Support\Facades\Auth;

class EmisorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()->TipoUsuario->tipo_usuario == 'Emisor')
            {
                return $next($request);
            }
        }
        return redirect('/home');
    }
}
