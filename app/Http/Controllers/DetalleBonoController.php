<?php

namespace App\Http\Controllers;

use App\Bono;
use App\DetalleBono;
use App\Http\Requests\StoreDetalleRequest;
use App\Services\BonoService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class DetalleBonoController extends Controller
{
    protected $bonoService;

    /**
     * @param BonoService $bonoService
     */
    public function __construct(BonoService $bonoService)
    {
        $this->bonoService = $bonoService;
        $this->middleware('auth');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $id -> id del bono
     * @return Response
     */
    public function store(StoreDetalleRequest $request, $id)
    {

        $this->bonoService->RegisterDetalleBono($request->all(),Bono::find($id));
        Session::flash('message', 'Se creó exitosamente la cuota!');
        return Redirect::action('BonosController@show',[$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        Session::flash('message', 'Se eliminó exitosamente el Bono!');

        return Redirect::action('BonosController@index',[$id]);
    }
}
