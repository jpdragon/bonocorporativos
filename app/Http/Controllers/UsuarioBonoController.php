<?php

namespace App\Http\Controllers;

use App\Bono;
use App\DetalleBono;
use App\Http\Requests\StoreUsuarioBonoRequest;
use app\Services\BonoService;
use App\User;
use App\UserBono;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UsuarioBonoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id_bono)
    {
        $userBono = UserBono::with('User')->where('id_bono', $id_bono)->where('id_user', '<>', Auth::User()->id)->get();
        Log::info($userBono);
        $id = $id_bono;
        return view('bonos.user.index', compact(['userBono', 'id']));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreUsuarioBonoRequest $request, $id_bono)
    {
        if(UserBono::where('id_bono', $id_bono)->where('id_user',Auth::User()->id)->first() == null)
        {
            return Redirect::action('UsuarioBonoController@index',[$id_bono]);
        }
        $userBono = new UserBono;
        $userBono->id_bono = $id_bono;
        //$id_user = User::find(['email' => $request->get('email')])->first();
        $id_user = User::where('email', $request->get('email'))->first();
        if($id_user == null)
        {
            Session::flash('message', 'El usuario no existe!');
            return Redirect::action('UsuarioBonoController@index',[$id_bono]);
        }
        else if(UserBono::where('id_bono',$id_bono)->where('id_user',$id_user->id)->first() != null)
        {
            Session::flash('message', 'El usuario ya esta en la lista!');
            return Redirect::action('UsuarioBonoController@index',[$id_bono]);

        }
        if($id_user->TipoUsuario->nombre == 'Emisor')
        {
            Session::flash('message', 'No se puede agregar a un Emisors');
            return Redirect::action('UsuarioBonoController@index',[$id_bono]);
        }
        Log::info($id_user);
        $userBono->id_user = $id_user->id;

        $userBono->save();
        Session::flash('message', 'Se añadió exitosamente el bonista: ' . $request->get('email') . '!');
        return Redirect::action('UsuarioBonoController@index',[$id_bono]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id_bono, $id)
    {
        //
        $userBono = UserBono::where('id_user', $id)->where('id_bono', $id_bono)->delete();
        Session::flash('message', 'Se eliminó exitosamente de la lista de bonistas!');
        return Redirect::action('UsuarioBonoController@index',[$id_bono]);
    }
}
