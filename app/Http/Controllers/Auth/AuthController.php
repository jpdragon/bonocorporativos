<?php

namespace App\Http\Controllers\Auth;

use App\TipoUsuario;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    /**
     * Create a new authentication controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:45',
            'apellidos' => 'required|max:45',
            'direccion' => 'required|max:45',
            'ciudad' => 'required|max:45',
            'pais' => 'required|max:45',
            'fecha_nacimiento' => 'required|max:45',
            'sexo' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ],
        [
            'required' => 'El campo :attribute es requerido.',
            'max' => 'El campo :attribute no debe tener más de :max caracteres',
            'email.unique' => 'El email ya se encuentra en uso',
            'email' => 'El email ingresado debe ser válido',
            'password.confirmed' => 'El campo contraseña no es igual en su confimación',
            'password.required' => 'El campo contraseña es requerido'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $tipo = TipoUsuario::firstOrCreate(['tipo_usuario' => 'Bonista']);
        return User::create([
            'nombre' => $data['nombre'],
            'apellidos' => $data['apellidos'],
            'direccion' => $data['direccion'],
            'ciudad' => $data['ciudad'],
            'pais' => $data['pais'],
            'fecha_nacimiento' => $data['fecha_nacimiento'],
            'sexo' => ($data['sexo']=='masculino') ? 1 : 0,
            'id_tipo' => $tipo->id,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
