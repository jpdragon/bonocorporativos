<?php

namespace App\Http\Controllers;

use App\Bono;
use App\DetalleBono;
use App\Http\Requests\StoreBonoRequest;
use App\Services\BonoService;
use App\UserBono;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class BonosController extends Controller
{

    //private BonoService $bonoService;
    protected $bonoService;

    /**
     * @param BonoService $bonoService
     */
    public function __construct(BonoService $bonoService)
    {
        $this->bonoService = $bonoService;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Bonos = Auth::user()->Bonos;
        $tipoUsuario = Auth::User()->TipoUsuario->tipo_usuario;
        return view('bonos.index', compact(['Bonos','tipoUsuario']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('bonos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(StoreBonoRequest $request)
    {
        $this->bonoService->RegisterBono($request->all());
        Session::flash('message', 'Se creó exitosamente el Bono!');
        return Redirect::action('BonosController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
        $bono = Bono::find($id);
        $detalles = $bono->DetalleBono;
        $tipoUsuario = Auth::User()->TipoUsuario->tipo_usuario;
        return view('bonos.show',compact(['bono','detalles','tipoUsuario']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //elimina todos los detalle bono con bono boista y bono emisor.
    }
}
