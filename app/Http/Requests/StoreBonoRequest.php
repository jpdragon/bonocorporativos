<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StoreBonoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->TipoUsuario->tipo_usuario == 'Emisor')
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'valor_nominal' => 'required|numeric',
            'valor_comercial' => 'required|numeric',
            'fecha_inicio' => 'required|date',
            'num_año' => 'required|integer',
            'dias_año' => 'required|integer',
            'dias_periodo' => 'required|integer',
            'total_periodos' => 'required|integer',
            'imp_renta' => 'required|numeric',
            'prima' => 'required|numeric',
            'estructuracion' => 'required|numeric',
            'colocacion' => 'required|numeric',
            'flotacion' => 'required|numeric',
            'cavali' => 'required|numeric',
            'tasa_descuento' => 'required|numeric',
            'tea' => 'required|numeric',

        ];
    }
}
