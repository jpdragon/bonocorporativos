<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\UserBono;
use Illuminate\Support\Facades\Auth;

class StoreUsuarioBonoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //quien puede guardar usuarios - bono
        //emisor
        if(Auth::user()->TipoUsuario->tipo_usuario == 'Emisor')
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'email'=>'required|email',
        ];
    }
}
