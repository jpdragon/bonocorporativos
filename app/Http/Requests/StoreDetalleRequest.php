<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StoreDetalleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //quien puede guardar un detalle de bono
        if(Auth::user()->TipoUsuario->tipo_usuario == 'Emisor')
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'inflacion_proyectada' => 'required|numeric',
            'plazo_gracia' => 'required|alpha|size:1',
        ];
    }
}
