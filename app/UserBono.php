<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBono extends Model
{
    //

    protected $table = 'user_bono';

    public function User()
    {
        return $this->hasOne('App\User','id','id_user');
    }
    public function Bono()
    {
        return $this->hasOne('App\Bono','id','id_bono');
    }
}
