<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre',
        'apellidos',
        'direccion',
        'ciudad',
        'pais',
        'fecha_nacimiento',
        'sexo',
        'email',
        'password',
        'id_tipo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password','id_tipo', 'remember_token'];


    public function TipoUsuario()
    {
        return $this->belongsTo('App\TipoUsuario','id_tipo');
    }

    public function Bonos()
    {
        return $this->belongsToMany('App\Bono', 'user_bono', 'id_user', 'id_bono');
    }

}
