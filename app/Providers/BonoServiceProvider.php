<?php

namespace App\Providers;

use App\Http\Controllers\BonosController;
use App\Services\BonoService;
use Illuminate\Support\ServiceProvider;

class BonoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(
            'BonoService', function($app) {
            return new BonoService();
        });
    }
}
